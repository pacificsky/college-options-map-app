# College Options Map Application

## About

This is an application designed to help high school students find colleges.
It was funded through by a federal grant through [UC Davis](https://www.ucdavis.edu/) [College Options](http://www.collegeoptions.org) under the supervision of Richard Meyers at [Enterprise High School](http://www.enterprisehornets.com/) in Redding California and developed by [Tyler Shuster](http://tyler.shuster.house) through [Pacific Sky Creative, Inc.](http://pacificsky.co). It is based on the [Electron](http://electron.atom.io) platform using the [Neutron](http://quaintous.com/neutron/) framework.

## Requirements

`node/npm`

## Installing

After cloning this repository, run `npm install` in the source directory. Run `npm start` to develop. Run `npm run packager` to build binaries for Windows, macOS, and Linux.
