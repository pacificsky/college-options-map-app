"use strict";

var colleges = [{
	name: "UC Davis",
	address: "1 Shields Ave., Davis, CA",
	lngLat: [-121.750944, 38.537281],
	video: "https://youtu.be/PAwB_t_iM7U",
	type: "UC"
}, {
	name: "UC Berkeley",
	address: "100 Academic Hall, Berkeley, CA",
	lngLat: [-122.272747, 37.871593],
	video: "https://youtu.be/u-KmgRRu_T0",
	type: "UC"
}, {
	name: "UC Merced",
	address: "5200 Lake Rd., Merced, CA",
	lngLat: [-120.43111, 37.363734],
	video: "https://youtu.be/0bS2Q_iSIhU",
	type: "UC"
}, {
	name: "UC San Francisco",
	address: "505 Parnassus Ave., San Francisco, CA",
	lngLat: [-122.457814, 37.76309],
	video: "https://youtu.be/80yQ8wTnBB4",
	type: "UC"
}, {
	name: "UC Santa Cruz",
	address: "1156 High St., Santa Cruz, CA",
	lngLat: [-122.054306, 36.97763],
	video: "https://youtu.be/2nfYTyUnfM0",
	type: "UC"
}, {
	name: "UC Santa Barbara",
	address: "Santa Barbara, CA  93106",
	lngLat: [-119.849698, 34.413587],
	video: "https://youtu.be/9QQM7MrFe5E?list=PLXEfGFwHwSKmfDSs2XOLoJ9944vAs1RuC",
	type: "UC"
}, {
	name: "UC Los Angeles",
	address: "Los Angeles, CA  90095",
	lngLat: [-118.444056, 34.070264],
	video: "https://youtu.be/g1e11lsrSvw",
	type: "UC"
}, {
	name: "UC Irvine",
	address: "Irvine, CA  92697",
	lngLat: [-117.842132, 33.647124],
	video: "https://youtu.be/kIEqAPd_jqE",
	type: "UC"
}, {
	name: "UC Riverside",
	address: "900 University Ave., Riverside, CA",
	lngLat: [-117.328143, 33.978881],
	video: "https://youtu.be/vkn2JqFYD6c",
	type: "UC"
}, {
	name: "UC San Diego",
	address: "9500 Gilman Dr., La Jolla, CA",
	lngLat: [-117.242258, 32.887912],
	video: "https://youtu.be/WYzKO3OKISs",
	type: "UC"
}, {
	name: "Simpson University",
	address: "2211 College View Dr., Redding, CA",
	lngLat: [-122.330382, 40.617845],
	video: "https://vimeo.com/43572438",
	type: "Private"
}, {
	name: "California College of the Arts",
	address: "5212 Broadway, Oakland, CA",
	lngLat: [-122.250819, 37.835448],
	video: "https://youtu.be/XtaZS-FSvKw",
	type: "Private"
}, {
	name: "Dominican University of California",
	address: "50 Acacia Ave., San Rafael, CA",
	lngLat: [-122.513491, 37.979879],
	video: "https://vimeo.com/74497205",
	type: "Private"
}, {
	name: "Holy Names University",
	address: "3500 Mountain Blvd., Oakland, CA",
	lngLat: [-122.186495, 37.80249],
	video: "https://youtu.be/wSFIXoZQcSA",
	type: "Private"
}, {
	name: "Mills College",
	address: "5000 MacArthur Blvd., Oakland, CA",
	lngLat: [-122.18411, 37.777375],
	video: "https://youtu.be/sYmizCKhoTA",
	type: "Private"
}, {
	name: "Notre Dame de Namur University",
	address: "1500 Ralston Ave., Belmont, CA",
	lngLat: [-122.286751, 37.517377],
	video: "https://youtu.be/wcF3hWxleFg",
	type: "Private"
}, {
	name: "Pacific Union University",
	address: "37 Miller Ave., Mill Valley, CA",
	lngLat: [-122.547003, 37.905181],
	video: "https://youtu.be/fpxrbqnU9Eo",
	type: "Private"
}, {
	name: "Saint Mary's College of California",
	address: "1928 St. Mary's Rd., Moraga, CA",
	lngLat: [-122.110135, 37.841347],
	video: "https://youtu.be/ulvTtLvqrTs?list=PLVGNzM9QnnXft6w9iz2ucepb7wvMif6VX",
	type: "Private"
}, {
	name: "Samuel Merritt University",
	address: "3100 Telegraph Ave., Oakland, CA",
	lngLat: [-122.266284, 37.820308],
	video: "http://samuelmerritt.mediacore.tv/media/samuel-merritt-university-30-second-overview",
	type: "Private"
}, {
	name: "San Francisco Art Institute",
	address: "800 Chestnut St., San Francisco, CA",
	lngLat: [-122.417183, 37.80371],
	video: "https://vimeo.com/137908289",
	type: "Private"
}, {
	name: "San Francisco Conservatory of Music",
	address: "50 Oak St., San Francisco, CA",
	lngLat: [-122.420384, 37.775559],
	video: "https://youtu.be/davsqKbszvA",
	type: "Private"
}, {
	name: "Santa Clara University ",
	address: "500 El Camino Real, Santa Clara, CA",
	lngLat: [-121.940608, 37.349952],
	video: "https://youtu.be/GGhOlaWk0OY",
	type: "Private"
}, {
	name: "Stanford University",
	address: "450 Serra Mall, Standford, CA",
	lngLat: [-122.170233, 37.427517],
	video: "https://youtu.be/UReueAK5gjM",
	type: "Private"
}, {
	name: "Touro University - California",
	address: "1310 Club Dr., Vallejo, CA",
	lngLat: [-122.265311, 38.084794],
	video: "https://youtu.be/cxz0m1z6pKw",
	type: "Private"
}, {
	name: "University of San Francisco",
	address: "Fulton St., San Francisco, CA 94117",
	lngLat: [-122.443957, 37.776125],
	video: "https://youtu.be/WZfaqKV2eC0",
	type: "Private"
}, {
	name: "Fresno Pacific University",
	address: "1717 S Chestnut Ave., Fresno, CA",
	lngLat: [-119.735767, 36.726351],
	video: "https://vimeo.com/9924460",
	type: "Private"
}, {
	name: "Humphreys College",
	address: "6650 Inglewood Ave., Stockton, CA",
	lngLat: [-121.316155, 38.010932],
	video: "https://youtu.be/Bfn7zGSG0GA",
	type: "Private"
}, {
	name: "University of the Pacific",
	address: "3601 Pacific Ave., Stockton, CA",
	lngLat: [-121.312419, 37.980055],
	video: "https://youtu.be/s0EM_DYJxPg?list=PLR7sPpWAZmeepPc_LphblxIy7a0c_lKwf",
	type: "Private"
}, {
	name: "William Jessup University",
	address: "2121 University Ave., Rocklin, CA",
	lngLat: [-121.292514, 38.819695],
	video: "https://vimeo.com/101460425",
	type: "Private"
}, {
	name: "California Lutheran University",
	address: "60 W Olsen Rd., Thousand Oaks, CA",
	lngLat: [-118.876873, 34.224781],
	video: "https://youtu.be/-BsLLM6e2sE",
	type: "Private"
}, {
	name: "Thomas Aquinas College",
	address: "10000 Ojai Santa Paula Rd., Santa Paula, CA",
	lngLat: [-119.086685, 34.429105],
	video: "https://youtu.be/ApjlfA6xzxg",
	type: "Private"
}, {
	name: "Westmont College",
	address: "955 La Paz Rd., Santa Barbara, CA",
	lngLat: [-119.659634, 34.447972],
	video: "https://vimeo.com/79699039",
	type: "Private"
}, {
	name: "American Academy of Dramatic Arts L.A.",
	address: "1336 N La Brea Ave., Los Angeles, CA",
	lngLat: [-118.343701, 34.095378],
	video: "https://youtu.be/moftfXb3BuA",
	type: "Private"
}, {
	name: "American Jewish University",
	address: "15600 Mulholland Dr., Bel-Air, CA",
	lngLat: [-118.472285, 34.127854],
	video: "https://youtu.be/YGFmQaB90_Q",
	type: "Private"
}, {
	name: "Antioch University Los Angeles",
	address: "400 Corporate Pointe, Culver City, CA",
	lngLat: [-118.384539, 33.988558],
	video: "https://youtu.be/YvJXJ7bmGVU",
	type: "Private"
}, {
	name: "Art Center College of Design",
	address: "1700 Lida St., Pasadena, CA",
	lngLat: [-118.18506, 34.168738],
	video: "https://youtu.be/Cj79fZDsj-Q",
	type: "Private"
}, {
	name: "Azusa Pacific University",
	address: "901 E Alosta Ave., Azusa, CA",
	lngLat: [-117.889024, 34.129194],
	video: "https://youtu.be/CK8CRNLqxGQ",
	type: "Private"
}, {
	name: "Biola University ",
	address: "13800 Biola Ave., La Mirada, CA",
	lngLat: [-118.017984, 33.905588],
	video: "https://youtu.be/yw6WMI7T9DA",
	type: "Private"
}, {
	name: "California Institute of Technology",
	address: "1200 E California Blvd., Pasadena, CA",
	lngLat: [-118.124514, 34.139011],
	video: "https://youtu.be/4PYLCvaMEWc",
	type: "Private"
}, {
	name: "California Institute of the Arts",
	address: "24700 McBean Pkwy., Valencia, CA",
	lngLat: [-118.567828, 34.392941],
	video: "https://youtu.be/bNJ7wrDavf0",
	type: "Private"
}, {
	name: "Claremont McKenna College",
	address: "888 Columbia Ave., Claremont, CA",
	lngLat: [-117.711183, 34.102082],
	video: "https://youtu.be/Lo91zrsIwWM?list=PL4orrodQTmUPiacpjDciGsVCIS3BIP7rf",
	type: "Private"
}, {
	name: "Harvey Mudd College",
	address: "301 Platt Blvd., Claremont, CA",
	lngLat: [-117.712125, 34.105891],
	video: "https://youtu.be/DF7Bj1QRdsg",
	type: "Private"
}, {
	name: "Loyola Marymount University",
	address: "1 Loyola Marymount University Dr., Los Angeles, CA",
	lngLat: [-118.422464, 33.966882],
	video: "https://youtu.be/qdraauX_m9M",
	type: "Private"
}, {
	name: "Marymount California University",
	address: "30800 Palos Verdes Dr. E, Rancho Palos Verdes, CA",
	lngLat: [-118.333547, 33.734989],
	video: "https://youtu.be/nmLK2Q4mevI",
	type: "Private"
}, {
	name: "The Master's College",
	address: "21726 Placerita Canyon Rd., Santa Clarita, CA",
	lngLat: [-118.517589, 34.381768],
	video: "https://youtu.be/91TO1ubo0p8",
	type: "Private"
}, {
	name: "Mount St. Mary's University",
	address: "12001 Chalon Rd., Los Angeles, CA",
	lngLat: [-118.482821, 34.085783],
	video: "https://youtu.be/WBQ0ziaT8Qk",
	type: "Private"
}, {
	name: "Occidental College",
	address: "Alumni Ave., Los Angeles, CA",
	lngLat: [-118.21665, 34.125301],
	video: "https://youtu.be/kxM-Dm-cjFk",
	type: "Private"
}, {
	name: "Otis College of Art + Design",
	address: "9045 Lincoln Blvd., Los Angeles, CA",
	lngLat: [-118.416958, 33.956673],
	video: "https://vimeo.com/138018297",
	type: "Private"
}, {
	name: "Pacific Oaks College",
	address: "55 W Eureka St., Pasadena, CA",
	lngLat: [-118.151486, 34.153055],
	video: "https://youtu.be/eg4hl0hzrWo",
	type: "Private"
}, {
	name: "Pepperdine University",
	address: "24255 Pacific Coast Hwy., Malibu, CA",
	lngLat: [-118.705735, 34.038975],
	video: "https://youtu.be/qMk8-Lx5TSM",
	type: "Private"
}, {
	name: "Pitzer College",
	address: "1050 N Mills Ave., Claremont, CA",
	lngLat: [-117.703866, 34.103654],
	video: "https://youtu.be/EQTo0SMq9tw",
	type: "Private"
}, {
	name: "Pomona College",
	address: "333 N College Wy., Claremont, CA",
	lngLat: [-117.713193, 34.097117],
	video: "https://youtu.be/3aF-ANf4KY8",
	type: "Private"
}, {
	name: "Scripps College",
	address: "1030 Columbia Ave., Claremont, CA",
	lngLat: [-117.711343, 34.10337],
	video: "https://youtu.be/SZSDz__4qA4",
	type: "Private"
}, {
	name: "So. California College of Health Sciences",
	address: "16200 Amber Valley Dr., Whittier, CA",
	lngLat: [-117.981794, 33.923498],
	video: "https://youtu.be/MuaVnVo1jEM",
	type: "Private"
}, {
	name: "University of La Verne",
	address: "1950 3rd St., La Verne, CA",
	lngLat: [-117.773235, 34.100854],
	video: "https://youtu.be/V9WbnkOzNyo",
	type: "Private"
}, {
	name: "University of Southern California",
	address: "Los Angeles, CA  90089-0911",
	lngLat: [-118.289205, 34.022013],
	video: "https://youtu.be/DAo3WiEK85w",
	type: "Private"
}, {
	name: "Whittier College",
	address: "13406 Philadelphia St., Whittier, CA",
	lngLat: [-118.032262, 33.978831],
	type: "Private"
}, {
	name: "Woodbury University",
	address: "7500 N Glenoaks Blvd., Burbank, CA",
	lngLat: [-118.341198, 34.208605],
	video: "https://youtu.be/wjeKdaUpF5o",
	type: "Private"
}, {
	name: "Chapman University",
	address: "1 University Dr., Orange, CA",
	lngLat: [-117.852575, 33.793083],
	video: "https://youtu.be/wjeKdaUpF5o",
	type: "Private"
}, {
	name: "Concordia University Irvine",
	address: "1530 Concordia, Irvine, CA",
	lngLat: [-117.812287, 33.654545],
	video: "https://youtu.be/7N6pjMIutjA",
	type: "Private"
}, {
	name: "Hope International University ",
	address: "2500 Nutwood Ave., Fullerton, CA",
	lngLat: [-117.885461, 33.876852],
	video: "https://youtu.be/MRGEYbD8rx4",
	type: "Private"
}, {
	name: "Laguna College of Art & Design",
	address: "2222 Laguna Canyon Rd., Laguna Beach, CA",
	lngLat: [-117.77266, 33.561318],
	video: "https://youtu.be/xojF0p15m8c",
	type: "Private"
}, {
	name: "Soka University",
	address: "1 Hope St., Aliso Viejo, CA",
	lngLat: [-117.732049, 33.555959],
	video: "https://youtu.be/khNdbDO-Mg0",
	type: "Private"
}, {
	name: "Vanguard University of Southern California",
	address: "55 Fair Dr., Costa Mesa, CA",
	lngLat: [-117.901311, 33.662922],
	video: "https://youtu.be/vp6gJhyEtSE",
	type: "Private"
}, {
	name: "California Baptist University",
	address: "8432 Magnolia Ave., Riverside, CA",
	lngLat: [-117.426801, 33.93148],
	video: "https://youtu.be/godNv7qNCT4",
	type: "Private"
}, {
	name: "La Sierra University",
	address: "4500 Riverwalk Pkwy., Riverside, CA",
	lngLat: [-117.500045, 33.910733],
	video: "https://vimeo.com/106455213",
	type: "Private"
}, {
	name: "Loma Linda University",
	address: "24851 Circle Dr., Loma Linda, CA",
	lngLat: [-117.259987, 34.052228],
	video: "https://youtu.be/86Az-H4C2aE",
	type: "Private"
}, {
	name: "University of Redlands",
	address: "1200 E Colton Ave., Redlands, CA",
	lngLat: [-117.16384, 34.063997],
	video: "https://youtu.be/wIvtoetJfI0",
	type: "Private"
}, {
	name: "Alliant International University",
	address: "1000 S Fremont Ave., Alhambra, CA",
	lngLat: [-118.15005, 34.081829],
	video: "https://youtu.be/d5aoYgwr6VE",
	type: "Private"
}, {
	name: "National University",
	address: "9388 Lightwave Ave., San Diego, CA",
	lngLat: [-117.127454, 32.829602],
	video: "http://content.jwplatform.com/previews/Ygf2mTne-4ciUk05F",
	type: "Private"
}, {
	name: "Point Loma Nazarene University",
	address: "3900 Lomaland Dr., San Diego, CA",
	lngLat: [-117.250055, 32.718299],
	video: "https://youtu.be/MyID0yCcuqg",
	type: "Private"
}, {
	name: "San Diego Christian College",
	address: "200 Riverview Pkwy., Santee, CA",
	lngLat: [-116.978417, 32.842433],
	video: "https://youtu.be/fMQWFZCS0Sg",
	type: "Private"
}, {
	name: "University of San Diego",
	address: "5998 Alcala Park, San Diego, CA",
	lngLat: [-117.183662, 32.77435],
	video: "https://video.sandiego.edu/app/Portal/video.aspx?PortalID=65a04c97-44ec-4a16-a05c-53cfb1aa7970&DestinationID=JeW6SlnMmkOVE7NS8SSX-Q&IsLivePreview=false&ContentID=G1irceFbyke-FQ7HPhcUfw&sharing=&credits=&attach=&links=&stats=&embed=&play=&hc=&cc=",
	type: "Private"
}, {
	name: "Allan Hancock College",
	address: "800 S College Dr., Santa Maria, CA",
	lngLat: [-120.421522, 34.943735],
	video: "https://youtu.be/BLqf0BibT9s",
	type: "Community College"
}, {
	name: "American River College",
	address: "4700 College Oak Dr., Sacramento, CA",
	lngLat: [-121.345496, 38.65041],
	video: "https://youtu.be/EStafZsRFkg",
	type: "Community College"
}, {
	name: "Antelope Valley College",
	address: "3041 W Ave. K, Lancaster, CA",
	lngLat: [-118.185373, 34.676169],
	video: "https://youtu.be/cyWRcC-G1jQ",
	type: "Community College"
}, {
	name: "Bakersfield College",
	address: "1801 Panorama Dr., Bakersfield, CA",
	lngLat: [-118.971648, 35.408],
	video: "https://youtu.be/TctTqqndh6Y",
	type: "Community College"
}, {
	name: "Barstow College",
	address: "2700 Barstow Rd., Barstow, CA",
	lngLat: [-117.023398, 34.870276],
	video: "https://youtu.be/VfG_DGZD71M",
	type: "Community College"
}, {
	name: "Berkeley City College",
	address: "2050 Center St., Berkeley, CA",
	lngLat: [-122.269382, 37.869659],
	video: "https://youtu.be/-WeghPYtJeE",
	type: "Community College"
}, {
	name: "Butte College",
	address: "3536 Butte Campus Dr., Oroville, CA",
	lngLat: [-121.64199, 39.650124],
	video: "https://youtu.be/F7lcfvDoKqg",
	type: "Community College"
}, {
	name: "Cabrillo College",
	address: "6500 Soquel Dr., Aptos, CA",
	lngLat: [-121.927039, 36.987276],
	video: "https://youtu.be/276CHdT7gR4",
	type: "Community College"
}, {
	name: "Ca�ada College",
	address: "4200 Farm Hill Blvd., Redwood City, CA",
	lngLat: [-122.266433, 37.447411],
	video: "https://youtu.be/9ihDN1IBCMU",
	type: "Community College"
}, {
	name: "Cerritos College",
	address: "11110 Alondra Blvd., Norwalk, CA",
	lngLat: [-118.096097, 33.886146],
	video: "https://youtu.be/TpS9nxluzSA",
	type: "Community College"
}, {
	name: "Cerro Coso Community College",
	address: "3000 College Heights Blvd., Ridgecrest, CA",
	lngLat: [-117.670908, 35.567336],
	type: "Community College"
}, {
	name: "Chabot College",
	address: "25555 Hesperian Blvd., Hayward, CA",
	lngLat: [-122.10685, 37.6398],
	video: "https://youtu.be/uwfsCEE9Uz0",
	type: "Community College"
}, {
	name: "Chaffey College",
	address: "5885 Haven Ave., Rancho Cucamonga, CA",
	lngLat: [-117.574831, 34.146636],
	type: "Community College"
}, {
	name: "Citrus College",
	address: "1000 W Foothill Blvd., Glendora, CA",
	lngLat: [-117.885237, 34.135488],
	type: "Community College"
}, {
	name: "City College of San Francisco",
	address: "50 Phelan Ave., San Francisco, CA",
	lngLat: [-122.451115, 37.725819],
	video: "https://youtu.be/6tVpzU3iXV8",
	type: "Community College"
}, {
	name: "Coastline Community College",
	address: "11460 Warner Ave., Fountain Valley, CA",
	lngLat: [-117.929139, 33.715663],
	type: "Community College"
}, {
	name: "College of Alameda",
	address: "555 Ralph Appezzato Memorial Pkwy., Alameda, CA",
	lngLat: [-122.279111, 37.78097],
	video: "https://youtu.be/eBhJBNkwZ7M",
	type: "Community College"
}, {
	name: "College of Marin",
	address: "835 College Ave., Kentfield, CA",
	lngLat: [-122.5496, 37.955595],
	video: "https://youtu.be/j1DRhAmPXZU",
	type: "Community College"
}, {
	name: "College of San Mateo",
	address: "1700 W Hillsdale Blvd., San Mateo, CA",
	lngLat: [-122.335123, 37.535051],
	video: "https://youtu.be/DkZ-cNnGn3M",
	type: "Community College"
}, {
	name: "College of the Canyons",
	address: "26455 Rockwell Canyon Rd., Santa Clarita, CA",
	lngLat: [-118.567696, 34.405063],
	video: "https://youtu.be/SU9Ll-Cd7Bg",
	type: "Community College"
}, {
	name: "College of the Desert",
	address: "43-500 Monterey Ave., Palm Desert, CA",
	lngLat: [-116.387543, 33.79817],
	video: "https://youtu.be/eVLeGDJ8IWI",
	type: "Community College"
}, {
	name: "College of the Redwoods",
	address: "7351 Tompkins Hill Rd., Eureka, CA",
	lngLat: [-124.195375, 40.698432],
	type: "Community College"
}, {
	name: "College of the Sequoias",
	address: "915 S Mooney Blvd., Visalia, CA",
	lngLat: [-119.314672, 36.323069],
	type: "Community College"
}, {
	name: "College of the Siskiyous",
	address: "800 College Ave., Weed, CA",
	lngLat: [-122.38937, 41.412615],
	type: "Community College"
}, {
	name: "Columbia College",
	address: "11600 Columbia College Dr., Sonora, CA",
	lngLat: [-120.38655, 38.030512],
	type: "Community College"
}, {
	name: "Contra Costa College",
	address: "2600 Mission Bell Dr., San Pablo, CA",
	lngLat: [-122.340449, 37.968654],
	video: "https://youtu.be/QQWNHwUJGW8",
	type: "Community College"
}, {
	name: "Copper Mountain College",
	address: "6162 Rotary Way, Joshua Tree, CA",
	lngLat: [-116.212879, 34.141484],
	type: "Community College"
}, {
	name: "Cosumnes River College",
	address: "8401 Center Pkwy, Sacramento, CA",
	lngLat: [-121.421649, 38.45358],
	video: "https://youtu.be/2fUIG1lDgH4",
	type: "Community College"
}, {
	name: "Crafton Hills College",
	address: "11711 Sand Canyon Rd., Yucaipa, CA",
	lngLat: [-117.102306, 34.038816],
	type: "Community College"
}, {
	name: "Cuesta College",
	address: "Highway 1, San Luis Obispo, CA",
	lngLat: [-120.679937, 35.248664],
	video: "https://youtu.be/EK-nBV1FXok",
	type: "Community College"
}, {
	name: "Cuyamaca College",
	address: "900 Rancho San Diego Pkwy, El Cajon, CA",
	lngLat: [-116.941028, 32.746504],
	video: "https://youtu.be/pRoxnFh3PB0",
	type: "Community College"
}, {
	name: "Cypress College",
	address: "9200 Valley View St., Cypress, CA",
	lngLat: [-118.025276, 33.828092],
	type: "Community College"
}, {
	name: "De Anza College",
	address: "21250 Stevens Creek Blvd., Cupertino, CA",
	lngLat: [-122.045055, 37.31954],
	type: "Community College"
}, {
	name: "Diablo Valley College",
	address: "321 Golf Club Rd., Pleasant Hill, CA",
	lngLat: [-122.071286, 37.968557],
	video: "https://youtu.be/d_GkrJOZthY",
	type: "Community College"
}, {
	name: "East Los Angeles College",
	address: "1301 Avenida Cesar Chavez, Monterey Park, CA",
	lngLat: [-118.150988, 34.041158],
	type: "Community College"
}, {
	name: "El Camino College",
	address: "16007 Crenshaw Blvd., Torrance, CA",
	lngLat: [-118.328442, 33.885236],
	type: "Community College"
}, {
	name: "El Camino Compton Center",
	address: "1111 E Artesia Blvd., Compton, CA",
	lngLat: [-118.209393, 33.87919],
	type: "Community College"
}, {
	name: "Evergreen Valley College",
	address: "3095 Yerba Buena Rd., San Jose, CA",
	lngLat: [-121.762782, 37.300119],
	type: "Community College"
}, {
	name: "Feather River College",
	address: "570 Golden Eagle Ave., Quincy, CA",
	lngLat: [-120.964938, 39.952135],
	video: "https://youtu.be/IVit9fgBsYs",
	type: "Community College"
}, {
	name: "Folsom Lake College",
	address: "10 College Prkwy., Folsom, CA",
	lngLat: [-121.129444, 38.662853],
	video: "https://youtu.be/2hcM8NE9Hq0",
	type: "Community College"
}, {
	name: "Foothill College",
	address: "12345 S El Monte Ave., Los Altos, CA",
	lngLat: [-122.113603, 37.367748],
	video: "https://youtu.be/SeUxYZZv8mw",
	type: "Community College"
}, {
	name: "Fresno City College",
	address: "1101 E University Ave., Fresno, CA",
	lngLat: [-119.796043, 36.768182],
	type: "Community College"
}, {
	name: "Fullerton College",
	address: "800 N State College Blvd., Fullerton, CA",
	lngLat: [-117.8835, 33.87911],
	video: "https://youtu.be/vBHaO7pXBKs",
	type: "Community College"
}, {
	name: "Gavilan College",
	address: "5055 Santa Teresa Blvd., Gilroy, CA",
	lngLat: [-121.56856, 36.973256],
	type: "Community College"
}, {
	name: "Glendale College",
	address: "1500 N Verdugo Rd., Glendale, CA",
	lngLat: [-118.228203, 34.168305],
	video: "https://youtu.be/-GIMOwlOk8k",
	type: "Community College"
}, {
	name: "Golden West College",
	address: "15744 Goldenwest St., Huntington Beach, CA",
	lngLat: [-118.005064, 33.732465],
	type: "Community College"
}, {
	name: "Grossmont College",
	address: "8800 Grossmont College Dr., El Cajon, CA",
	lngLat: [-117.00628, 32.816582],
	type: "Community College"
}, {
	name: "Hartnell College",
	address: "411 Central Ave., Salinas, CA",
	lngLat: [-121.665672, 36.673988],
	type: "Community College"
}, {
	name: "Imperial Valley College",
	address: "380 E Aton Rd., Imperial, CA",
	lngLat: [-115.505429, 32.829038],
	type: "Community College"
}, {
	name: "Irvine Valley College",
	address: "5500 Irvine Center Dr., Irvine, CA",
	lngLat: [-117.77499, 33.676109],
	type: "Community College"
}, {
	name: "Lake Tahoe Community College",
	address: "1 College Way, South Lake Tahoe, CA",
	lngLat: [-119.973228, 38.92737],
	type: "Community College"
}, {
	name: "Laney College",
	address: "900 Fallon St., Oakland, CA",
	lngLat: [-122.263146, 37.796865],
	video: "https://youtu.be/Wn7u20NBTzg",
	type: "Community College"
}, {
	name: "Las Positas College",
	address: "3000 Campus Hill Dr., Livermore, CA",
	lngLat: [-121.800476, 37.71086],
	type: "Community College"
}, {
	name: "Lassen College",
	address: "478-200 CA-139, Susanville, CA",
	lngLat: [-120.63056, 40.429826],
	video: "https://youtu.be/SdbY2ikMVKA",
	type: "Community College"
}, {
	name: "Long Beach City College - Liberal Arts",
	address: "4901 E Carson St., Long Beach, CA",
	lngLat: [-118.136201, 33.830355],
	video: "https://youtu.be/K8UQxY4GLSY?list=PLyY_q6_iJD2PrBsjiuwgcJ5THveYiRxoK",
	type: "Community College"
}, {
	name: "Los Angeles City College",
	address: "855 N Vermont Ave., Los Angeles, CA",
	lngLat: [-118.292785, 34.089233],
	video: "https://youtu.be/YYNPCVK4lcc",
	type: "Community College"
}, {
	name: "Los Angeles Harbor College",
	address: "1111 Figueroa Pl., Wilmington, CA",
	lngLat: [-118.283715, 33.784208],
	type: "Community College"
}, {
	name: "Los Angeles Mission College",
	address: "12890 Harding St., Sylmar, CA",
	lngLat: [-118.412708, 34.308585],
	type: "Community College"
}, {
	name: "Los Angeles Pierce College",
	address: "6201 Winnetka Ave., Woodland Hills, CA",
	lngLat: [-118.573289, 34.183655],
	type: "Community College"
}, {
	name: "Los Angeles Southwest College",
	address: "1600 W Imperial Ave., Los Angeles, CA",
	lngLat: [-118.306264, 33.93072],
	video: "https://youtu.be/vGX2xNGyhOw",
	type: "Community College"
}, {
	name: "Los Angeles Trade-Tech College",
	address: "400 W Washington Blvd., Los Angeles, CA",
	lngLat: [-118.269903, 34.032734],
	type: "Community College"
}, {
	name: "Los Angeles Valley College",
	address: "5800 Fullton Ave., Valley Glen, CA",
	lngLat: [-118.420144, 34.175706],
	type: "Community College"
}, {
	name: "Los Medanos College",
	address: "2700 E Leland Rd., Pittsburg, CA",
	lngLat: [-121.860501, 38.005334],
	video: "https://youtu.be/cbBacoTtRrk",
	type: "Community College"
}, {
	name: "Mendocino College",
	address: "1000 Hensley Creek Rd., Ukiah, CA",
	lngLat: [-123.228863, 39.188981],
	video: "https://youtu.be/UMCrk66_4Js",
	type: "Community College"
}, {
	name: "Merced College",
	address: "3600 M St., Merced, CA",
	lngLat: [-120.473019, 37.333679],
	type: "Community College"
}, {
	name: "Merritt College",
	address: "12500 Campus Dr., Oakland, CA",
	lngLat: [-122.165612, 37.789553],
	type: "Community College"
}, {
	name: "MiraCosta College",
	address: "1 Barnard Dr., Oceanside, CA",
	lngLat: [-117.302163, 33.190042],
	type: "Community College"
}, {
	name: "Mission College",
	address: "3000 Mission College Blvd., Santa Clara, CA",
	lngLat: [-121.982046, 37.391216],
	type: "Community College"
}, {
	name: "Modesto Junior College",
	address: "435 College Ave., Modesto, CA",
	lngLat: [-121.009725, 37.652067],
	type: "Community College"
}, {
	name: "Monterey Peninsula College",
	address: "980 Fremont St., Monterey, CA",
	lngLat: [-121.886847, 36.589318],
	type: "Community College"
}, {
	name: "Moorpark College",
	address: "7075 Campus Rd., Moorpark, CA",
	lngLat: [-118.835152, 34.301755],
	type: "Community College"
}, {
	name: "Moreno Valley College",
	address: "16130 Lasselle St., Moreno Valley, CA",
	lngLat: [-117.204621, 33.886138],
	type: "Community College"
}, {
	name: "Mt. San Antonio College",
	address: "1100 N Grand Ave., Walnut, CA",
	lngLat: [-117.845118, 34.048212],
	type: "Community College"
}, {
	name: "Mt. San Jacinto College",
	address: "1499 N State St., San Jacinto, CA",
	lngLat: [-116.96941, 33.806622],
	type: "Community College"
}, {
	name: "Napa Valley College",
	address: "2277 Napa Valley Hwy., Napa, CA",
	lngLat: [-122.277746, 38.274306],
	type: "Community College"
}, {
	name: "Norco College",
	address: "2001 Third St., Norco, CA",
	lngLat: [-117.568995, 33.91683],
	video: "https://youtu.be/3ZsRvkf7FOk",
	type: "Community College"
}, {
	name: "Ohlone College",
	address: "43600 Mission Blvd., Fremont, CA",
	lngLat: [-121.91361, 37.531063],
	type: "Community College"
}, {
	name: "Orange Coast College",
	address: "2701 Fairview Rd., Costa Mesa, CA",
	lngLat: [-117.908993, 33.669777],
	video: "https://youtu.be/aY0wwwf4PB8",
	type: "Community College"
}, {
	name: "Oxnard College",
	address: "4000 S Rose Ave., Oxnard, CA",
	lngLat: [-119.158354, 34.165193],
	type: "Community College"
}, {
	name: "Palo Verde College",
	address: "1 College Dr., Blythe, CA",
	lngLat: [-114.652692, 33.662565],
	type: "Community College"
}, {
	name: "Palomar College",
	address: "1140 W Mission Rd., San Marcos, CA",
	lngLat: [-117.186408, 33.148708],
	type: "Community College"
}, {
	name: "Pasadena City College",
	address: "1570 E Colorado Blvd., Pasadena, CA",
	lngLat: [-118.119168, 34.143933],
	type: "Community College"
}, {
	name: "Porterville College",
	address: "100 E College Ave., Porterville, CA",
	lngLat: [-119.014369, 36.049215],
	type: "Community College"
}, {
	name: "Reedley College",
	address: "995 N Reed Ave., Reedley, CA",
	lngLat: [-119.46109, 36.606385],
	type: "Community College"
}, {
	name: "Rio Hondo College",
	address: "3600 Workman Mill Rd., Whittier, CA",
	lngLat: [-118.033971, 34.018429],
	video: "https://youtu.be/rONuCCanUJU",
	type: "Community College"
}, {
	name: "Riverside City College",
	address: "4800 Magnolia Ave., Riverside, CA",
	lngLat: [-117.381502, 33.971178],
	type: "Community College"
}, {
	name: "Sacramento City College",
	address: "3835 Freeport Blvd., Sacramento, CA",
	lngLat: [-121.486843, 38.539541],
	type: "Community College"
}, {
	name: "Saddleback College",
	address: "38000 Margueritte Pkwy., Mission Viejo, CA",
	lngLat: [-117.673212, 33.542603],
	type: "Community College"
}, {
	name: "San Bernardino Valley College",
	address: "701 S Mt. Vernon Ave., San Bernardino, CA",
	lngLat: [-117.312953, 34.088754],
	type: "Community College"
}, {
	name: "San Diego City College",
	address: "1313 Park Blvd., San Diego, CA",
	lngLat: [-117.152771, 32.717491],
	type: "Community College"
}, {
	name: "San Diego Mesa College",
	address: "7250 Mesa College Dr., San Diego, CA",
	lngLat: [-117.163334, 32.805281],
	type: "Community College"
}, {
	name: "San Diego Miramar College",
	address: "10440 Black Mountain Rd., San Diego, CA",
	lngLat: [-117.122259, 32.906517],
	type: "Community College"
}, {
	name: "San Joaquin Delta College",
	address: "5151 Pacific Ave., Stockton, CA",
	lngLat: [-121.319324, 37.995364],
	video: "https://youtu.be/Np7tjRNFKGQ?list=PLeGyqSJEhAi9njKKeY-zDFJfrNW478C82",
	type: "Community College"
}, {
	name: "San Jose City College",
	address: "2100 Moorpark Ave., San Jose, CA",
	lngLat: [-121.930026, 37.315505],
	type: "Community College"
}, {
	name: "Santa Ana College",
	address: "1530 W 17th St., Santa Ana, CA",
	lngLat: [-117.886469, 33.759441],
	type: "Community College"
}, {
	name: "Santa Barbara City College",
	address: "721 Cliff Dr., Santa Barbara, CA",
	lngLat: [-119.695257, 34.406827],
	video: "https://youtu.be/Iml4zuspZgI",
	type: "Community College"
}, {
	name: "Santa Monica College",
	address: "1900 Pico Blvd., Santa Monica, CA",
	lngLat: [-118.473295, 34.017155],
	video: "https://youtu.be/XRnDJ4hyUno",
	type: "Community College"
}, {
	name: "Santa Rosa Junior College",
	address: "1501 Mendocino Ave., Santa Rosa, CA",
	lngLat: [-122.718387, 38.454958],
	video: "https://youtu.be/rTKpGsl0B9M",
	type: "Community College"
}, {
	name: "Santiago Canyon College",
	address: "8045 E Chapman Ave., Orange, CA",
	lngLat: [-117.766768, 33.79546],
	video: "https://youtu.be/I0IQ6JPq4X8",
	type: "Community College"
}, {
	name: "Shasta College",
	address: "11555 Old Oregon Trail, Redding, CA",
	lngLat: [-122.317854, 40.624183],
	video: "https://youtu.be/55MIYdwn-mg?list=PLaSWGbt2OWt2qGZvQjP9BQLLeNQjAxbKC",
	type: "Community College"
}, {
	name: "Sierra College",
	address: "5000 Rocklin Rd., Rocklin, CA",
	lngLat: [-121.210865, 38.790958],
	type: "Community College"
}, {
	name: "Skyline College",
	address: "3300 College Dr., San Bruno, CA",
	lngLat: [-122.46721, 37.630907],
	video: "https://youtu.be/6a2IPeMW8VI",
	type: "Community College"
}, {
	name: "Solano Community College",
	address: "4000 Suison Valley Rd., Fairfield, CA",
	lngLat: [-122.123542, 38.234829],
	video: "https://youtu.be/WogC9OFfhW0",
	type: "Community College"
}, {
	name: "Southwestern College",
	address: "880 National City Blvd., Chula Vista, CA",
	lngLat: [-117.10691, 32.675152],
	type: "Community College"
}, {
	name: "Taft College",
	address: "29 Cougar Ct., Taft, CA",
	lngLat: [-119.456508, 35.142467],
	type: "Community College"
}, {
	name: "Ventura College",
	address: "4667 Telegraph Rd., Ventura, CA",
	lngLat: [-119.230684, 34.277384],
	type: "Community College"
}, {
	name: "Victor Valley College",
	address: "18422 Bear Valley Rd., Victorville, CA",
	lngLat: [-117.266903, 34.473205],
	type: "Community College"
}, {
	name: "West Hills College Coalinga",
	address: "300 W Cherry Ln., Coalinga, CA",
	lngLat: [-120.356995, 36.14747],
	type: "Community College"
}, {
	name: "West Hills College Lemoore",
	address: "555 College Ave., Lemoore, CA",
	lngLat: [-119.821739, 36.290017],
	type: "Community College"
}, {
	name: "West Los Angeles College",
	address: "9000 Overland Ave., Culver City, CA",
	lngLat: [-118.386199, 34.002273],
	video: "https://youtu.be/_nU0orHIziE",
	type: "Community College"
}, {
	name: "West Valley College",
	address: "14000 Fruitvale Ave., Saratoga, CA",
	lngLat: [-122.010895, 37.264068],
	type: "Community College"
}, {
	name: "Woodland Community College",
	address: "2300 E Gibson Rd., Woodland, CA",
	lngLat: [-121.73543, 38.660407],
	type: "Community College"
}, {
	name: "Yuba College",
	address: "2088 N Beale Rd., Marysville, CA",
	lngLat: [-121.539676, 39.12655],
	type: "Community College"
}, {
	name: "Cal Maritime",
	address: "200 Maritime Academy Dr., Vallejo, CA",
	lngLat: [-122.23248, 38.069261],
	type: "CSU"
}, {
	name: "Cal Poly Pomona",
	address: "3801 W Temple Ave., Pomona, CA",
	lngLat: [-117.824478, 34.057751],
	type: "CSU"
}, {
	name: "Cal Poly San Luis Obispo",
	address: "San Luis Obispo, CA  93407",
	lngLat: [-120.663129, 35.3042],
	type: "CSU"
}, {
	name: "CSU Bakersfield",
	address: "9001 Stockdale Hwy., Bakersfield, CA",
	lngLat: [-119.103157, 35.351426],
	type: "CSU"
}, {
	name: "CSU Channel Islands",
	address: "1 University Dr., Camarillo, CA",
	lngLat: [-119.043463, 34.162056],
	type: "CSU"
}, {
	name: "CSU Chico",
	address: "400 W 1st St., Chico, CA",
	lngLat: [-121.846346, 39.727929],
	type: "CSU"
}, {
	name: "CSU Dominguez Hills",
	address: "1000 E Victoria St., Carson, CA",
	lngLat: [-118.254543, 33.862466],
	type: "CSU"
}, {
	name: "CSU East Bay",
	address: "25800 Carlos Bee Blvd., Hayward, CA",
	lngLat: [-122.057296, 37.656255],
	type: "CSU"
}, {
	name: "CSU Fresno",
	address: "5241 N Maple Ave., Fresno, CA",
	lngLat: [-119.746223, 36.810828],
	type: "CSU"
}, {
	name: "CSU Fullerton",
	address: "800 N State College Blvd., Fullerton, CA",
	lngLat: [-117.8835, 33.87911],
	type: "CSU"
}, {
	name: "CSU Long Beach",
	address: "1250 Bellflower Blvd., Long Beach, CA",
	lngLat: [-118.114395, 33.777043],
	type: "CSU"
}, {
	name: "CSU Los Angeles",
	address: "5151 State University Dr., Los Angeles, CA",
	lngLat: [-118.173614, 34.062192],
	type: "CSU"
}, {
	name: "CSU Monterey Bay",
	address: "100 Campus Center, Seaside, CA",
	lngLat: [-121.801764, 36.65445],
	type: "CSU"
}, {
	name: "CSU Northridge",
	address: "18111 Nordhoff St., Northridge, CA",
	lngLat: [-118.528165, 34.237387],
	type: "CSU"
}, {
	name: "CSU Sacramento",
	address: "6000 J St., Sacramento, CA",
	lngLat: [-121.428169, 38.56349],
	type: "CSU"
}, {
	name: "CSU San Bernardino",
	address: "5500 University Prkwy., San Bernardino, CA",
	lngLat: [-117.32179, 34.183524],
	type: "CSU"
}, {
	name: "CSU San Marcos",
	address: "333 S Twin Oaks Valley Rd., San Marcos, CA",
	lngLat: [-117.159738, 33.129114],
	type: "CSU"
}, {
	name: "CSU Stanislaus",
	address: "1 University Cir., Turlock, CA",
	lngLat: [-120.857156, 37.524005],
	type: "CSU"
}, {
	name: "Humboldt State University",
	address: "1 Harpst St., Arcata, CA",
	lngLat: [-124.079172, 40.876964],
	type: "CSU"
}, {
	name: "San Diego State University",
	address: "5500 Campanile Dr., San Diego, CA",
	lngLat: [-117.072665, 32.772133],
	type: "CSU"
}, {
	name: "San Francisco State University",
	address: "1600 Holloway Ave., San Francisco, CA",
	lngLat: [-122.476717, 37.722769],
	type: "CSU"
}, {
	name: "San Jose State University",
	address: "1 Washington Sq., San Jose, CA",
	lngLat: [-121.881276, 37.335142],
	type: "CSU"
}, {
	name: "Sonoma State University",
	address: "1801 E Cotati Ave., Rohnert Park, CA",
	lngLat: [-122.673491, 38.339263],
	type: "CSU"
}];