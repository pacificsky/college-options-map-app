'use strict';

/* eslint no-undef: 0*/
/* eslint max-len: ["error", 150]*/
/* eslint no-loop-func: 0*/
var fs = require('fs');
var colleges = [];
var collegeObj = {};
var maxPanes = 7;
var activeColleges = [];
var queryingVisible = false;
var path = require('path');

fs.readFile(path.join(__dirname, '/js/colleges.json'), function (error, data) {
  if (error) {
    console.log('error', error);
  } else {
    colleges = JSON.parse(data);
    colleges.sort(function (a, b) {
      var nameA = a.properties.title.toUpperCase();
      var nameB = b.properties.title.toUpperCase();
      var returnVal = 0;
      if (nameA < nameB) {
        returnVal = -1;
      } else if (nameA > nameB) {
        returnVal = 1;
      }
      return returnVal;
    });
    colleges.forEach(function (college) {
      if (college.properties.video) {
        if (college.properties.video.indexOf('youtube.com') > -1) {
          college.properties.video += '?enablejsapi=1';
        }
      }
      collegeObj[slugify(college.properties.title)] = college;
    });
  }
});

mapboxgl.accessToken = 'pk.eyJ1IjoidHlsZXJzaHVzdGVyIiwiYSI6IkVtQUNDR0kifQ.lmw4IoRwovnroo6vfHO9gQ';

var map = new mapboxgl.Map({
  container: 'map',
  style: 'mapbox://styles/tylershuster/ciql1s3dx001bblkm3k8e3mbg',
  minZoom: 6.5,
  maxZoom: 14,
  zoom: 6.5,
  pitch: getPitchFromZoom(6.5),
  center: [-121.497803, 38.662638],
  attributionControl: false,
  maxBounds: new mapboxgl.LngLatBounds(new mapboxgl.LngLat(-126, 30), new mapboxgl.LngLat(-110, 42))
});

map.dragRotate.disable();
map.touchZoomRotate.disableRotation();

map.on('zoom', function (event) {
  if (event.originalEvent) {
    var zoom = map.getZoom();
    map.setPitch(getPitchFromZoom(zoom));

    var eastMax = (111.5 + (zoom - 7) * 0.5) * -1;
    map.setMaxBounds(new mapboxgl.LngLatBounds(new mapboxgl.LngLat(-126, 30), new mapboxgl.LngLat(eastMax, 42)));
  }
}).on('zoomend', function () {
  if (queryingVisible) {
    populateVisible();
  }
}).on('click', function (event) {
  var features = map.queryRenderedFeatures(event.point);
  features.forEach(function (feature) {
    if (feature.layer.id === 'colleges') {
      collegeModal(feature);
      map.flyTo({
        center: feature.geometry.coordinates,
        zoom: 14,
        pitch: getPitchFromZoom(14)
      });
    } else if (feature.layer.id === 'cluster-count') {
      if (map.getZoom() >= 10) {
        map.flyTo({
          center: feature.geometry.coordinates,
          zoom: 14,
          pitch: getPitchFromZoom(14)
        });
      } else {
        map.flyTo({
          center: feature.geometry.coordinates,
          zoom: 10,
          pitch: getPitchFromZoom(10)
        });
      }
    }
  });
}).on('moveend', function () {
  if (queryingVisible) {
    populateVisible();
  }
}).on('rotate', function () {
  var bearing = map.getBearing();
  $('#north').css('transform', 'rotateZ(' + bearing * -1 + 'deg)');
  if (queryingVisible) {
    populateVisible();
  }
}).on('load', function () {
  map.addSource('colleges', {
    type: 'geojson',
    data: {
      type: 'FeatureCollection',
      features: colleges
    },
    cluster: true,
    clusterRadius: 30
  });
  var layers = [[10, '#ff9912'], [5, '#0c2a35'], [0, '#53b0d2']];
  layers.forEach(function (layer, i) {
    map.addLayer({
      id: 'cluster-' + i,
      type: 'circle',
      source: 'colleges',
      paint: {
        'circle-color': 'rgba(12, 42, 53, 0.75)',
        'circle-radius': 26
      },
      filter: i === 0 ? ['>=', 'point_count', layer[0]] : ['all', ['>=', 'point_count', layer[0]], ['<', 'point_count', layers[i - 1][0]]]
    });
  });
  map.addLayer({
    id: 'colleges',
    type: 'symbol',
    source: 'colleges',
    layout: {
      'icon-image': "{marker-symbol}-map",
      'icon-offset': [0, -20],
      'icon-size': 0.8,
      'text-field': '{title}',
      'text-font': ['Lato Regular', 'Arial Unicode MS Bold'],
      'text-offset': [0, 0.6],
      'text-anchor': 'top'
    }
  });
  map.addLayer({
    id: 'cluster-count',
    type: 'symbol',
    source: 'colleges',
    layout: {
      'text-field': '{point_count}',
      'text-font': ['Lato Bold', 'Arial Unicode MS Bold'],
      'text-size': 20
    },
    paint: {
      'text-color': '#ffffff'
    }
  });
  var pane = document.querySelectorAll('.list--wrapper')[0];
  colleges.forEach(function (college, index) {
    var collegeCard = document.createElement('SPAN');
    collegeCard.innerHTML = college.properties.title;
    collegeCard.dataset.id = index;
    collegeCard.dataset.textId = slugify(college.properties.title);
    collegeCard.dataset.collegeType = college.properties.collegeType;
    pane.appendChild(collegeCard);
  });
  $('.college--list').addClass('active');
});

$('body').on('click', '.college--list span', function (event) {
  map.flyTo({
    center: colleges[event.target.dataset.id].geometry.coordinates,
    zoom: 14,
    pitch: getPitchFromZoom(14)
  });
}).on('hover', '.college--list span', function (event) {
  activeCollege = event.target.dataset.id;
}).on('click', '.list--filter li', function (event) {
  var collegeType = event.currentTarget.dataset.collegeType;
  var source = map.getSource('colleges');
  $('.filter--active').removeClass('filter--active');
  if (collegeType === 'all') {
    source.setData({ type: 'FeatureCollection', features: colleges });
    $('.college--list span').removeClass('hidden');
    queryingVisible = false;
  } else if (collegeType === 'visible') {
    source.setData({ type: 'FeatureCollection', features: colleges });
    $(event.currentTarget).addClass('filter--active');
    queryingVisible = true;
    populateVisible();
  } else {
    var newData = [];
    queryingVisible = false;
    $(event.currentTarget).addClass('filter--active');
    $('.college--list span[data-college-type="' + collegeType + '"]').removeClass('hidden');
    $('.college--list span:not([data-college-type="' + collegeType + '"])').addClass('hidden');
    colleges.forEach(function (college) {
      if (college.properties.collegeType === collegeType) {
        newData.push(college);
      }
    });
    source.setData({ type: 'FeatureCollection', features: newData });
  }
  var listTitle = document.getElementById('list--title');
  switch (collegeType) {
    case 'all':
      listTitle.innerHTML = 'All Colleges';
      break;
    case 'visible':
      listTitle.innerHTML = 'Visible Colleges';
      break;
    case 'private':
      listTitle.innerHTML = 'Private Universities';
      break;
    case 'uc':
      listTitle.innerHTML = 'UC Institutions';
      break;
    case 'csu':
      listTitle.innerHTML = 'CSU Institutions';
      break;
    case 'community':
      listTitle.innerHTML = 'Community Colleges';
      break;
    default:
      listTitle.innerHTML = '';
      break;
  }
}).on('click', '.list--toggle', function () {
  $('.college--list').toggleClass('active');
}).on('click', '.pane--close', function (event) {
  var collegePane = $(event.target).parents('.college--pane')[0];
  var isClosed = $(collegePane).hasClass('closed');
  var collegeName = slugify(collegePane.dataset.collegeName);
  $(collegePane).addClass('exiting');
  setTimeout(function () {
    $(collegePane).addClass('exited');
    setTimeout(function () {
      $(collegePane).remove();
      activeColleges.splice(activeColleges.indexOf(collegeName), 1);
      if (!isClosed) {
        $('.college--pane:last-of-type').removeClass('closed');
      }
      checkComparePane();
    }, 500);
  }, 500);
}).on('click', '#map--recenter', function () {
  map.easeTo({
    zoom: 6.5,
    bearing: 0,
    pitch: getPitchFromZoom(6.5),
    duration: 4000,
    center: [-121.497803, 38.662638]
  });
}).on('click', '.college--pane.closed', function (event) {
  if (!$(event.target).hasClass('pane--close')) {
    $('.college--pane').addClass('closed');
    $(event.currentTarget).removeClass('closed');
  }
}).on('click', '.compare--wrapper', function (event) {
  if ($(event.target).hasClass('compare--wrapper')) {
    $('#college--pane-wrapper').removeClass('closed');
    $('.college--list').addClass('active');
    $('body').removeClass('modal-active');
    $('.compare--wrapper').fadeOut(500, function () {
      $('.compare--wrapper').remove();
    });
  }
}).on('click', '.compare--toggle', function () {
  compareColleges();
}).on('click', '.college--video', function (event) {
  var collegeName = $(event.target).parents('.college--pane').data('college-name');
  var iframe = document.createElement('IFRAME');
  var modal = document.createElement('DIV');
  modal.className = 'modal--video';
  iframe.id = 'video--' + slugify(collegeName);
  iframe.src = event.target.dataset.url;
  $(modal).css('display', 'none');
  modal.appendChild(iframe);
  document.body.appendChild(modal);
  $(modal).fadeIn(500, function () {
    $(modal).fadeIn();
  });
  $('body').addClass('modal-active');
  setTimeout(function () {
    if (event.target.dataset.url.indexOf('youtube.com') > -1) {
      callPlayer(iframe.id, 'playVideo');
    } else if (event.target.dataset.url.indexOf('vimeo.com') > -1) {
      var player = new Vimeo.Player(iframe);
      player.play();
    }
  }, 1000);
}).on('click', '.modal--video', function (event) {
  if ($(event.target).hasClass('modal--video')) {
    $('body').removeClass('modal-active');
    $('.modal--video').fadeOut(500, function () {
      $('.modal--video').remove();
    });
  }
}).on('click', '#info', function () {
  $('body').addClass('modal-active');
  var aboutWrapper = document.createElement('DIV');
  aboutWrapper.id = 'wrapper--about';
  var aboutModal = document.createElement('DIV');
  aboutModal.id = 'modal--about';
  aboutModal.innerHTML = '\n<h2>About</h2>\n<p>This program was developed through a grant through UC Davis College Options for Enterprise High School in Redding, CA.</p>\n<p>All code written by employees at Pacific Sky: Tyler Shuster, Matt Christensen, &amp; Jesse Moffett</p>\n<img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /><br /><span xmlns:dct="http://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/InteractiveResource" property="dct:title" rel="dct:type">College Options Map Application</span> by tyler@pacificsky.co is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.\n';
  aboutWrapper.appendChild(aboutModal);
  document.body.appendChild(aboutWrapper);
}).on('click', '#wrapper--about', function (event) {
  if ($(event.target).is('#wrapper--about')) {
    $('body').removeClass('modal-active');
    $(event.target).fadeOut(500, function () {
      $(event.target).remove();
    });
  }
});

/**
 * Opens the modal for a given college
 * @param  {[type]} college [description]
 * @param  {[type]} options =             {} [description]
 */
function collegeModal(college) {
  if (activeColleges.indexOf(slugify(college.properties.title)) < 0) {
    var wrapper = document.getElementById('college--pane-wrapper');
    var pane = document.createElement('DIV');
    pane.className = 'college--pane entering';
    var paneBody = document.createElement('DIV');
    pane.dataset.collegeName = college.properties.title;
    var paneHeader = document.createElement('HEADER');
    paneHeader.innerHTML = '    <span class="pane--close"></span>    <h1>' + college.properties.title + '</h1>\n    <i class="icon--' + college.properties.collegeType + '"></i>';
    pane.appendChild(paneHeader);
    var degreesOffered = document.createElement('SPAN');
    degreesOffered.className = 'college--info';
    degreesOffered.innerHTML = '<span class="info--title">Degrees Offered:</span> ';
    college.properties['degrees-offered'].split(',').forEach(function (degree, index, array) {
      degreesOffered.innerHTML += degree;
      if (index + 1 !== array.length) {
        degreesOffered.innerHTML += ', ';
      }
    });
    paneBody.appendChild(degreesOffered);
    paneBody.innerHTML += '    <span class="college--info">      <span class="info--title">Student Population: </span>      ' + commaSeparateNumber(college.properties['student-population']) + '    </span>';
    if (college.properties['student-ratio']) {
      paneBody.innerHTML += '\n      <span class="college--info">        <span class="info--title">Student Ratio: </span>        ' + college.properties['student-ratio'] + ' to 1      </span>';
    }
    paneBody.innerHTML += '    <span class="college--info">      <span class="info--title">Campus Setting: </span>      ' + college.properties['city-size'] + '    </span>';
    if (college.properties['religious-affiliation']) {
      paneBody.innerHTML += '      <span class="college--info">        <span class="info--title">Religious Affiliation: </span>        ' + college.properties['religious-affiliation'] + '      </span>';
    }
    paneBody.innerHTML += '    <span class="college--info">      <span class="info--title">Campus Housing: </span>      ' + (college.properties['campus-housing'] ? 'Yes' : 'No') + '    </span>';
    if (college.properties['tution-2016']) {
      paneBody.innerHTML += '      <span class="college--info">        <span class="info--title">2016 Tuition: </span>        $' + commaSeparateNumber(college.properties['tution-2016']) + '      </span>';
    }
    if (college.properties['housing-2016']) {
      paneBody.innerHTML += '      <span class="college--info">        <span class="info--title">2016 Housing: </span>        $' + commaSeparateNumber(college.properties['housing-2016']) + '      </span>';
    }
    if (college.properties['expenses-on-campus-2016']) {
      paneBody.innerHTML += '      <span class="college--info">        <span class="info--title">2016 On-Campus Expenses: </span>        $' + commaSeparateNumber(college.properties['expenses-on-campus-2016']) + '      </span>';
    }
    if (college.properties['expenses-off-campus-2016']) {
      paneBody.innerHTML += '      <span class="college--info">        <span class="info--title">2016 Off-Campus Expenses: </span>        $' + commaSeparateNumber(college.properties['expenses-off-campus-2016']) + '      </span>';
    }
    if (college.properties['average-scholarship']) {
      paneBody.innerHTML += '      <span class="college--info">        <span class="info--title">Average Scholarship: </span>        $' + commaSeparateNumber(college.properties['average-scholarship']) + '      </span>';
    }
    if (college.properties.video) {
      paneBody.innerHTML += '<span class="college--info college--video" data-url="' + college.properties.video + '">Watch Video</span>';
    }
    pane.appendChild(paneBody);
    if (activeColleges.length < maxPanes) {
      wrapper.appendChild(pane);
      setTimeout(function () {
        $('.college--pane').addClass('closed');
        $(pane).removeClass('entering').removeClass('closed');
      }, 500);
      activeColleges.push(slugify(college.properties.title));
    } else {
      maxCollegesModal();
    }
  } else {
    $('.college--pane').addClass('closed');
    $('[data-college-name="' + college.properties.title + '"]').removeClass('closed');
  }
  checkComparePane();
}

/**
 * Informs the user they cannot open more modals
 */
function maxCollegesModal() {
  var modal = document.createElement('DIV');
  modal.className = 'modal--max-colleges';
  modal.innerHTML = 'You may only compare ' + maxPanes + ' colleges at a time. Please close at least one.';
  document.body.appendChild(modal);
  $(modal).fadeOut(5000, function () {
    $(modal).remove();
  });
}

/**
 * Checks if there are any active colleges and opens or closes the pane
 */
function checkComparePane() {
  if (activeColleges.length < 1) {
    $('#college--pane-wrapper').addClass('closed');
  } else {
    if (activeColleges.length > 1) {
      $('#college--pane-wrapper').addClass('compare--active');
    } else {
      $('#college--pane-wrapper').removeClass('compare--active');
    }
    $('.compare--number').text(activeColleges.length);
    $('#college--pane-wrapper').removeClass('closed');
  }
}

/**
 * Opens the college comparison table modal
 */
function compareColleges() {
  var tableWrapper = document.createElement('DIV');
  tableWrapper.className = 'compare--wrapper';
  var comparisonTable = document.createElement('TABLE');
  comparisonTable.className = 'college--compare';
  var rows = {
    'degrees-offered': [],
    'student-population': [],
    'student-ratio': [],
    'city-size': [],
    'religious-affiliation': [],
    'campus-housing': [],
    'tuition-2016': [],
    'housing-2016': [],
    'expenses-on-campus-2016': [],
    'expenses-off-campus-2016': [],
    'average-scholarship': []
  };
  var tableHeader = comparisonTable.createTHead();
  var headerRow = tableHeader.insertRow(0);
  var legendCell = headerRow.insertCell(0);
  legendCell.innerHTML += '<img src=\'' + __dirname + '/img/compare-icon.svg\'>';
  activeColleges.forEach(function (collegeSlug, index) {
    var college = collegeObj[collegeSlug];
    var headerTitle = headerRow.insertCell(index + 1);
    headerTitle.innerHTML += '<img src=\'' + __dirname + '/img/college-' + college.properties.collegeType + '-map.svg\'>';
    headerTitle.innerHTML += college.properties.title;
    for (var property in rows) {
      if (rows.hasOwnProperty(property)) {
        if (college.properties.hasOwnProperty(property)) {
          var value = '';
          switch (property) {
            case 'degrees-offered':
              value = college.properties[property].join(', ');
              break;
            case 'student-population':
              value = commaSeparateNumber(college.properties[property]);
              break;
            case 'student-ratio':
              value = college.properties[property] ? college.properties[property] + ' to 1' : '';
              break;
            case 'city-size':
              value = college.properties[property];
              break;
            case 'religious-affiliation':
              value = college.properties[property];
              break;
            case 'campus-housing':
              value = college.properties[property] ? 'Yes' : 'No';
              break;
            case 'tuition-2016':
              if (college.properties[property] === 0 || college.properties[property] === -1) {
                value = 'N/A';
              } else {
                value = '$' + commaSeparateNumber(college.properties[property]);
              }
              break;
            case 'housing-2016':
              if (college.properties[property] === 0 || college.properties[property] === -1) {
                value = 'N/A';
              } else {
                value = '$' + commaSeparateNumber(college.properties[property]);
              }
              break;
            case 'expenses-on-campus-2016':
              if (college.properties[property] === 0 || college.properties[property] === -1) {
                value = 'N/A';
              } else {
                value = '$' + commaSeparateNumber(college.properties[property]);
              }
              break;
            case 'expenses-off-campus-2016':
              if (college.properties[property] === 0 || college.properties[property] === -1) {
                value = 'N/A';
              } else {
                value = '$' + commaSeparateNumber(college.properties[property]);
              }
              break;
            case 'average-scholarship':
              if (college.properties[property] === 0 || college.properties[property] === -1) {
                value = 'N/A';
              } else {
                value = '$' + commaSeparateNumber(college.properties[property]);
              }
              break;
            default:
              value = college.properties[property];
              break;
          }
          rows[property].push(value);
        } else {
          rows[property].push('');
        }
      }
    }
  });
  var tbody = document.createElement('TBODY');
  for (var property in rows) {
    if (rows.hasOwnProperty(property)) {
      var row = tbody.insertRow(0);
      var header = row.insertCell(0);
      var label = '';
      switch (property) {
        case 'degrees-offered':
          label = 'Degrees Offered';
          break;
        case 'student-population':
          label = 'Student Population';
          break;
        case 'student-ratio':
          label = 'Student Ratio';
          break;
        case 'city-size':
          label = 'City Size';
          break;
        case 'religious-affiliation':
          label = 'Religious Affiliation';
          break;
        case 'campus-housing':
          label = 'Campus Housing';
          break;
        case 'tuition-2016':
          label = 'Tuition 2016';
          break;
        case 'housing-2016':
          label = 'Housing 2016';
          break;
        case 'expenses-on-campus-2016':
          label = 'Expenses On Campus 2016';
          break;
        case 'expenses-off-campus-2016':
          label = 'Expenses Off Campus 2016';
          break;
        case 'average-scholarship':
          label = 'Average Scholarship';
          break;
        default:
          label = property;
          break;
      }
      header.innerHTML += label;
      rows[property].forEach(function (column, index) {
        var cell = row.insertCell(index + 1);
        cell.innerHTML += '' + column;
      });
    }
  }
  comparisonTable.appendChild(tbody);
  tableWrapper.appendChild(comparisonTable);
  $(tableWrapper).css('display', 'none');
  $('#college--pane-wrapper').addClass('closed');
  $('.college--list').removeClass('active');
  document.body.appendChild(tableWrapper);
  setTimeout(function () {
    $(tableWrapper).fadeIn();
    $('body').addClass('modal-active');
  });
}

/**
 * Populates the college list pane with visible colleges
 */
function populateVisible() {
  var bounds = map.getBounds();
  var north = bounds.getNorth();
  var south = bounds.getSouth();
  var east = bounds.getEast();
  var west = bounds.getWest();
  $('.college--list span').addClass('hidden');
  colleges.forEach(function (college) {
    var lng = college.geometry.coordinates[0];
    var lat = college.geometry.coordinates[1];
    if (lng > west && lng < east && lat > south && lng < north) {
      var title = slugify(college.properties.title);
      $('.college--list span[data-text-id="' + title + '"]').removeClass('hidden');
    }
  });
}