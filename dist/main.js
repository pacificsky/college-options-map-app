'use strict';

var electron = require('electron');
var app = electron.app;
var BrowserWindow = electron.BrowserWindow;
var mainWindow = void 0;
var path = require('path');

/**
 * Creates the main window
 */
function createWindow() {
  mainWindow = new BrowserWindow({
    'title': 'College Options',
    'width': 1920,
    'height': 1080,
    'frame': false,
    'fullscreen': true,
    'icon': path.join(__dirname, '/img/app-icon.png'),
    'web-preferences': { plugins: true }
  });
  mainWindow.loadURL('file://' + __dirname + '/index.html');
  // mainWindow.webContents.openDevTools()
  mainWindow.on('closed', function () {
    mainWindow = null;
  });
  mainWindow.webContents.on('new-window', function (event) {
    event.preventDefault();
  });
}
app.commandLine.appendSwitch('--disable-pinch');

app.on('ready', createWindow);

app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', function () {
  if (mainWindow === null) {
    createWindow();
  }
});