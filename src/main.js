const electron = require('electron');
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;
let mainWindow;
const path = require('path');

/**
 * Creates the main window
 */
function createWindow() {
  mainWindow = new BrowserWindow({
    'title': 'College Options',
    'width': 1920,
    'height': 1080,
    'frame': false,
    'fullscreen': true,
    'icon': path.join(__dirname, '/img/app-icon.png'),
    'web-preferences': {plugins: true}
  });
  mainWindow.loadURL(`file://${__dirname}/index.html`);
  // mainWindow.webContents.openDevTools()
  mainWindow.on('closed', () => {
    mainWindow = null;
  });
  mainWindow.webContents.on('new-window', event => {
    event.preventDefault();
  });
}
app.commandLine.appendSwitch('--disable-pinch');

app.on('ready', createWindow);

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow();
  }
});
